package com.dinsaren.springjwtauthentication.repository;

import com.dinsaren.springjwtauthentication.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

}

