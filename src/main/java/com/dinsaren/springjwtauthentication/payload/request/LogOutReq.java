package com.dinsaren.springjwtauthentication.payload.request;

public class LogOutReq {
    private Integer userId;

    public Integer getUserId() {
        return this.userId;
    }
}
